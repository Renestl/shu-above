# "Bakery PSD website template" Inspired Website

[Live demo]()

This project was inspired by [Bakery](https://freebiesbug.com/psd-freebies/bakery-psd-website-template/) ([alt link](https://dribbble.com/shots/2847987-Shot-Bakery/attachments/586121))

No code or assets were used from the original. The original was used as inspiration. All code was written from scratch using HTML5, CSS3 and Sass.

## Photo Credits

Photo by [Jon Flobrant](https://unsplash.com/photos/VHlrCYpJGEY) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
Photo by [Ethan Weil](https://unsplash.com/photos/khVRCHG6GLE)
Photo by [Seb](https://unsplash.com/photos/0bw7KSaeaP4?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
Photo by [American Public Power Association](https://unsplash.com/photos/-TwF1STzFz8?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Sven Brandsma](https://unsplash.com/photos/Glbh-AKaPiw) on Unsplash
Photo by [abzakcd](https://unsplash.com/photos/42huiJdVX6s) on Unsplash
Photo by [Petya Boyadzhieva](https://unsplash.com/photos/ZMS5PPn_zEo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Master Wen](https://unsplash.com/photos/AX-ma0j6elM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Jason Blackeye](https://unsplash.com/photos/KUgDg__TMGk) on Unsplash
Photo by [Annabelle Tipper](https://unsplash.com/photos/he1zXYz2e4E?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Nathan McBride](https://unsplash.com/photos/mokWXKenVoY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Cassie Boca](https://unsplash.com/photos/y_EsP2gbocQ) on Unsplash
Photo by [Karsten Würth (@inf1783)](https://unsplash.com/photos/0w-uTa0Xz7w?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Shaun Dakin](https://unsplash.com/photos/nY_RHD44e_o?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash
Photo by [Johny Goerend](https://unsplash.com/photos/hoSRVDlrRKg) on Unsplash
Photo by [Karsten Koehn](https://unsplash.com/photos/Z8i3uY_Uy-w?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on Unsplash

## Other
[Wind Farm](https://en.wikipedia.org/wiki/Wind_farm)