document.addEventListener("DOMContentLoaded", function() {
	console.log('ready');
	carousel();
});



// LISTINGS CAROUSEL
var carousel = function() {

	// Determine if element is available
	if (!document.querySelector || 
		!('classList' in document.body)) {
	return false;
	}

	// Read necessary elements from the DOM once
	var box = document.querySelector('.carouselbox');
	var next = box.querySelector('.next');
	var prev = box.querySelector('.prev');

	// Define global counter, items and current item
	var slides = box.querySelectorAll('.slides li');
	var counter = 0;
	var amount = slides.length;
	var current = slides[0];

	box.classList.add('active');

	// navigate carousel
	function navigate(direction) {

		// hide the old current list item from view
		current.classList.remove('current');

		// get the new element position
		counter = counter + direction;

		if(direction === -1 && counter < 0) {
			counter = amount - 1;
		}
		if(direction === 1 && !slides[counter]) { 
			counter = 0;
		}

		// set new current and add css class
		current = slides[counter];
		current.classList.add('current');
	}

	// add event handlers to the buttons
	next.addEventListener('click', function(e) {
		navigate(1);
	})
	prev.addEventListener('click', function(e) {
		navigate(-1);
	});

	// show the intial element
	navigate(0);
};